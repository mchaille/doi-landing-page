<div class="container-fluid">

	<div class="row">
	 	<div class="col-12 col-sm-4 blueBackground" > <img id="logo" src="./src/esrfLogo.png"> </img> </div>
		<div class="col-12 col-sm-8 blueBackground flex"> 
			<div class="col-12 whiteVCenteredFont text-right" > {investigation.doi} </div>
		</div>
        </div>
	<div class="row">
	 	<div class="col-8" > 
		 	<label>Title</label>
		 	<p class="grayBackground"> {investigation.summary} </p>
		 	
		 	<label> Abstract </label>
		 	<p class="grayBackground">  </p>
		 	
		 		<p>
		 		<label> Experimental report </label> 
		 		<button type="button" class="btn btn-primary btn-lg btn-block blueBackground disabled" style='background-color:#D7D7D7; border:1px solid #D7D7D7;'>Download experimental report </button> 
		 		</p>
		 		
		 		<p>
		 		<label> Download data </label>
		 		<button type="button" class="btn btn-primary btn-lg btn-block blueBackground disabled" style='background-color:#D7D7D7; border:1px solid #D7D7D7;'>Download data </button>
		 		</p>
	 	</div>
	 	<div class="col-4">
	 		
	 		    <div class="row"> 
	 		          <div class="col-12" >
	 		          	<label> Identifier </label>
	 		          	<p> {investigation.id}</p>
	 		          	<label> Authors </label>
	 		          	
	 		           {#investigation.investigationUsers}
	 		          
	 		           		{@eq key=role value="Principal investigator"} 
	 		           	        <p>{.user.fullName}</p>  
	 		           		{/eq}
	 		           			
	 		           	    {@eq key=role value="Proposal scientist"}
	 		           	        <p>{.user.fullName}</p>
	 		           	    {/eq}
	 		           {/investigation.investigationUsers}
	 		          	 
	 		          	<label> Publisher </label>
	 		          	<p> ESRF </p>
	 		          	<label> Publication year </label>
	 		          	<p> year </p>
	 		          	<label> Proposal number </label>
	 		          	<p> {investigation.name} </p>          	
	 		          </div>
	 		    </div>
	 	</div>
	</div>
</div>
      
