function ICATClient(){
    this.server = "https://linfalcon.esrf.fr:8181";
    this.sessionId = null;

    this.connection = {
          plugin : 'simple',
          credentials : [
              {
                  username : 'root',
                  password :'root'
              }
          ]
    };            

}

ICATClient.prototype.connect = function(doi){	    
		var _this = this; 
        if (!this.sessionId){
            $.ajax({		
                url: this.server + "/icat/session",		
                data: { json : JSON.stringify(this.connection) },		
                type: "post",		
                dataType : "json",		
                success: function( data ) {			
                    _this.sessionId = data.sessionId;                                      
                    _this.getByDOI(doi);

                    
                },			
                error: function() {
                    alert( "Sorry, there was a problem!" );
                }
            });
        }

};
 
ICATClient.prototype.getByDOI = function(doi){	   
        var _this = this;    
        var query = "select inv from Investigation inv where inv.doi='" + doi + "' INCLUDE inv.investigationInstruments invInstruments, inv.investigationUsers invUser, invUser.user, invInstruments.instrument" ;			
		$.ajax({		
			url: this.server + "/icat/entityManager",		
			data: {
                    sessionId: _this.sessionId,
                    server : this.server,
                    query : query  
			},
			type: "GET",
			dataType : "json",
			success: function( data ) {				
                
                if ( typeof data[0] !== "undefined" )
                {
                    dust.render('esrf-landing-page', 
                        {
                        investigation : data[0].Investigation,
                        investigationUsers : data[0].Investigation.investigationUsers
                        }, 
                        function(err, out) {                        
                            $("#main").html(out);
                        });
                }
                else {
                    alert("The DOI number " + doi + " was not found !");
                }
			},
			// Code to run if the request fails
			error: function() {
		 		alert( "Sorry, error while processing the DOI request !" );
			}
		});
}; 

