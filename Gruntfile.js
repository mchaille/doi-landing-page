module.exports = function(grunt) {

  grunt.initConfig({
    jshint: {
      files: ['Gruntfile.js', 'src/*.js', 'test/**/*.js'],
      options: {
        globals: {
          jQuery: true
        }
      }
    }, 
    watch: {
      files: ['<%= jshint.files %>', 'templates/**js'],
      tasks: [ 'uglify','dustjs','wiredep']
    },
    uglify: {
     options: {
         mangle: true
    },
       my_target: {
          files: {
             'min/doi-landing-page.min.js': [
               'bower_components/jquery/dist/jquery.js',
               'bower_components/bootstrap/dist/js/bootstrap.js',
               'bower_components/dustjs-linkedin/dist/dust-full.min.js',
               'bower_components/dustjs-helpers/dist/dust-helpers.min.js',
               'src/*js']
          }
       }
    },
    wiredep: {
      target: {
        src: 'index.html' 
      }
    },
    cssmin: {
        my_target: {
            src: 'css/*.css',
            dest: 'min/output.min.css'
        }
    },

   dustjs : {
                    compile : {
                        files : {
                            'min/precompiled.templates.min.js' : [ 'templates/**js' ]
                        }
                    }
                }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-wiredep');
  grunt.loadNpmTasks('grunt-dustjs');
  grunt.loadNpmTasks('grunt-css');
  grunt.registerTask('default', ['jshint', 'dustjs', 'uglify', 'cssmin', 'wiredep']);

};
